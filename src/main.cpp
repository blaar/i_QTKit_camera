//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blQTKit.h"
#include "interactive.hpp"
#include "silent.hpp"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "blc_core.h"
#include "blc_channel.h"
#include "blc_command.h"
#include "blc_program.h"
#include "blc_image.h"

/**
 @main
 Exemple of using the console to display the camera
 */

char const *output_name=NULL;

char const *output_pipe_name;
double period;
int iteration=1;
struct timeval timestamp={};
char const *force_ansi=NULL;
int scale;
blc_channel channel;
int synchronous_mode=1;
int initialised=0;
int pipe_out;
char const *output_format_option, *output_type_option;
char const *downscale;


void init_pipe()
{
    if (output_pipe_name){
        fprintf(stderr, "Waiting connection on pipe '%s'\n", output_pipe_name);
        freopen(output_pipe_name, "w", stdout);
    }
    if (isatty(STDOUT_FILENO)) pipe_out=0;
    else {
        pipe_out=1;
        printf("%s\n", channel.name);
        fflush(stdout);
    }
}

int capture_callback(blc_array *image, void *){
    blc_mem mem;
    uint32_t output_type, output_format;
    int width, height;
    blc_array output_def;
    
    if (!initialised)
    {
        if (output_type_option != NULL) output_type = STRING_TO_UINT32(output_type_option);
        else output_type = image->type;
        if (output_format_option != NULL) output_format = STRING_TO_UINT32(output_format_option);
        else output_format = image->format;
        width=image->dims[image->dims_nb-2].length;
        height=image->dims[image->dims_nb-1].length;
        
        if (downscale) {
            width/=2;
            height/=2;
        }
        
        blc_image_def(&output_def, output_type, output_format, width,  height); //A simplifier
        channel.create_or_open(output_name, BLC_CHANNEL_WRITE, output_def.type, output_def.format, output_def.dims_nb, output_def.dims);
        blc_array_destroy(&output_def);
        channel.publish();
        initialised = 1;
        blc_command_loop_init(0);
    }
    
    if (blc_command_loop_start()){
 //       channel.begin_write();
        if (downscale) blc_image_downscale(&channel, image);
        else blc_image_convert(&channel, image);
 //       channel.end_write();
        blc_command_loop_end();
        return 1;
    }
    else return 0;
}

static void on_quit(){
    channel.~blc_channel();
}

int main(int argc, char **argv){
    
    blc_program_add_option(&output_format_option, 'f', "format", "Y800", "set ouput video format", NULL);
    blc_program_add_option(&output_name, 'o', "output", "blc_channel", "output channel", NULL);
    blc_program_add_option(&downscale, 'D', "downscale", NULL, "Downscale by 2x2 the image", NULL);
    blc_program_add_option(&output_type_option, 't', "type", "FL32", "set ouput data type", NULL);
    blc_program_init(&argc, &argv, on_quit);
    
    if (output_name==NULL) asprintf((char**)&output_name, "/%s%d", blc_program_name, getpid());
    
    init_capture(capture_callback, NULL);
    return 0;
}
